#coding :utf-8
#author :mixian
#email  :951930122@qq.com


'''
第1部分
OS
'''
# import os
#
# FileAbsPath = os.path.abspath('7.6demo1.py')
# print(FileAbsPath)
# print(type(FileAbsPath),f'\n')
#
# # 判断当前文件是否是文件夹
# FileName = os.path.isdir(f"D:\new python campus") # bool
# print(FileName)
#
# FileNameDir = os.path.isfile(f"D:\new python campus") # bool
# print(FileNameDir)
#
# #获取当前的绝对路径
# print(os.getcwd())
#
# #获取当前列表的所有文件
# print(os.listdir(f"D:/new python campus/")) # 如果是转义 / \\ r
#
# # 获取当前路径下的结尾名字
# print(os.path.basename(r'D:\new python campus\7.6demo1.py'))
#
# file = os.path.split(r"D:\new python campus\7.6demo1.py")[-1]
# # 切割路径
# print("===",file)
#
# # 如何拼接路径
# print(os.path.join("aa","bb","cc","x.txt"))
#
# # 返回文件大小
# print(os.path.getsize(r"D:\new python campus\7.6demo1.py")) # 字节 ，1个字节 =8个二进制位




'''
第2部分
sys
'''
import sys

# 打印执行的函数参数
# 1首先使用cmd的命令行 win+r
# 2切换路径 cd 切换路径 cd c: cd .. 退出当前目录
# 3 进入到我们的项目目录 cd Demo 打一个d 按tab 自动补全 完成 填充
# 4 进入到命令行 cd Demo  dir 列出当前列表的元素
#  5 输入 python Demo13 txt txt2
# 最后打印出我携带的参数列表
# print(sys.argv[0])
# print(sys.argv[1])

# version 打印我们的python
print(sys.version) # 打印python的版本

# #打印操作系统平台版本
# print(sys.platform)
#
# # python 的版权信息
# print(sys.copyright)
#
# # executable :查看解释器的安装路径
# print(sys.executable)
#
# # python
# print(sys.path)
#
# # exit
#
# from sys import *
# # 输入输出
# sys.stdout.write("第一行文本")
# sys.stdin.read("第一行文本")


'''
第三部分

'''

# # 快速生产一个列表 [1,2,3,4,5,6,7,8,9]
# list = []
# for i in range(10):
#     list.append(i)
# print(list)
#
# # 如何只用一行代码来实现 =》推导式来生成
# list_data = [x for x in range(10)]
# print(list_data)
#
# # 延续一下列表推导式
# list_data1 = [x for x in range(10) if x%2==0]
# print(list_data1)
#
# # 如果我要输出格式[(1,2),(3,4),(5,6)]
# list_data2 = [(x,y) for x in range(5) for y in range(10)]
# print(list_data2)
#
# # 字典生成
# # {"name":"xianghao","age":23,"dept":"computer"}
# dic_data3 = {x:x for x in {"name","age","dept"}}
# dic_data3["name"] = "zhansan"
# print(dic_data3)
#
# # 列表
# list_data4 = ["name","age","dept"]
# list_data5 = ["zhansan",19,"computer"]
# dic_data6 = {x:y for x,y in zip(list_data4,list_data5)}#zip函数的使用
# print(dic_data6)


'''
第4部分
迭代器
'''

# #  迭代器是容器执行的可迭代过程 => 容器在for循环的可执行过程
# # 首先我们看到的迭代器就是可迭代的对象,迭代器拥有迭代对象的所有特征
# # 迭代器的方法主要有两个  __iter__ next__
# # 迭代器一蹴而就的能记住对象的位置 for核心
# # 迭代器是一个惰性机制, 你不动,我不懂,你叫我动 我才懂,
# # 可迭代对象不是值得某一类的数据,而是之的手存储了某特定元素的容器对象
# # python 中有哪些容器 list tuple rang
#
# list = [1,2,3,4]
#
#
# new_list = iter(list)
# next(new_list)
# print(next(new_list))
# print(next(new_list))
# print(next(new_list))
# # print(next(new_list))
# # print(next(new_list))
# #

'''
第5部分
装饰器
'''
# def hi(name="hi函数"):
#     return "这里是 " + name
#
#
# print(hi())
# # output: 'hi yasoob'
#
# # 我们甚至可以将一个函数赋值给一个变量，比如
# greet = hi
# # 我们这里没有在使用小括号，因为我们并不是在调用hi函数
# # 而是在将它放在greet变量里头。我们尝试运行下这个
#
# print(greet())
# # output: 'hi yasoob'
#
# # 如果我们删掉旧的hi函数，看看会发生什么！
# # del hi//此处为深拷贝
# # print(hi())
# # # outputs: NameError
#
# print(greet())
# # outputs: 'hi yasoob' # 注意:1.copy 什么叫深拷贝 浅拷贝
